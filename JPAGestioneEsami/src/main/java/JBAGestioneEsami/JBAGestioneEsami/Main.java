package JBAGestioneEsami.JBAGestioneEsami;

import java.util.List;
import java.util.Scanner;

import javax.persistence.Query;

public class Main {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		//System.out.println("Creo Studente.");
		//createStudente(scanner);
		//System.out.println("Cerco Studente.");
		//readStudente(scanner);
		//System.out.println("Modifico Studente.");
		//updateStudente(scanner);
		//System.out.println("Elimina Studente. ");
		//deleteStudente(scanner);
		//System.out.println("Creo Esame Studente.");
		//createEsame(scanner);
		System.out.println("Cerco Esame Studente.");
		readEsame(scanner);
		System.out.println("Elimino Esame Studente. ");
		deleteEsame(scanner);
	}

	public static void createStudente(Scanner input) {

		Studente studente = new Studente();
		System.out.println("Aggiungi Studente.");

		System.out.println("Inserisci Nome:");
		String nome = input.nextLine();
		studente.setNome(nome);

		System.out.println("Inserisci Cognome: ");
		String cognome = input.nextLine();
		studente.setCognome(cognome);

		System.out.println("Inserisci Data Di Nascita: ");
		String dataDiNascita = input.nextLine();
		studente.setDataDiNascita(dataDiNascita);

		ServicesCrud crud_services = new ServicesCrud("jpa-example");

		// Creazione record in tabella
		crud_services.jpaCreate(studente);

		crud_services.closeLogicaJPA();
	}

	public static void readStudente(Scanner input) {

		System.out.println("Cerca Studente. ");
		System.out.println("Seleziona Matricola: ");
		int matricola = input.nextInt();
		input.nextLine();

		ServicesCrud crud_services = new ServicesCrud("jpa-example");

		Query jpaRead = crud_services.jpaRead("SELECT s FROM Studente s WHERE matricola = " + matricola);

		Studente studente = (Studente) jpaRead.getSingleResult();
		System.out.println(studente);
		crud_services.closeLogicaJPA();

	}

	public static void updateStudente(Scanner input) {


		System.out.println("Modifica Studente. ");
		System.out.println("Seleziona Matricola: ");
		int matricola = input.nextInt();
		input.nextLine();

		ServicesCrud crud_services = new ServicesCrud("jpa-example");
		Query jpaRead = crud_services.jpaRead("SELECT s FROM Studente s WHERE matricola = " + matricola);
		
		Studente studente = (Studente) jpaRead.getSingleResult();
		System.out.println(studente);
		
		System.out.println("Quale campo vuoi modificare?");
		System.out.println("1) Nome - 2) Cognome - 3) Data di Nascita - 4) Uscire");
		int key = input.nextInt();
		input.nextLine();
		int uscita = 0;
		while (uscita != 4) {
			switch (key) {
			case 1://nome
				System.out.println("Inserisci Nuovo Nome:");
				String nome = input.nextLine();
				studente.setNome(nome);
				break;

			case 2://cognome
				System.out.println("Inserisci Nuovo Cognome:");
				String cognome = input.nextLine();
				studente.setCognome(cognome);
				break;

			case 3://dataDiNascita
				System.out.println("Inserisci Nuova Data di Nascita:");
				String dataDiNascita = input.nextLine();
				studente.setDataDiNascita(dataDiNascita);
				break;

			case 4:
				uscita = 4;
				break;
			}
		}
		crud_services.jpaUpdate(studente);
		System.out.println("Dati Studente Aggiornati.");
		crud_services.closeLogicaJPA();
	}
	
	public static void deleteStudente(Scanner input){
		
		System.out.println("Elimina Studente. ");
		System.out.println("Seleziona Matricola: ");
		int matricola = input.nextInt();
		input.nextLine();

		ServicesCrud crud_services = new ServicesCrud("jpa-example");
		Query jpaRead = crud_services.jpaRead("SELECT s FROM Studente s WHERE matricola = " + matricola);
		
		Studente studente = (Studente) jpaRead.getSingleResult();
		
		crud_services.jpaDelete(studente);
		System.out.println("Studente Eliminato.");
		crud_services.closeLogicaJPA();
	}
	
	public static void createEsame(Scanner input){
		
		
		
		Esame esame = new Esame();
		
		System.out.println("Aggiungi Esame.");
		System.out.println("Seleziona Studente: ");
		int matricola = input.nextInt();
		input.nextLine();

		ServicesCrud crud_services = new ServicesCrud("jpa-example");
		Query jpaRead = crud_services.jpaRead("SELECT s FROM Studente s WHERE matricola = " + matricola);
		
		Studente studente = (Studente) jpaRead.getSingleResult();
		

		System.out.println("Inserisci Nome Esame:");
		String nome = input.nextLine();
		esame.setNome(nome);

		System.out.println("Inserisci Crediti Esame: ");
		int crediti = input.nextInt();
		input.nextLine();
		esame.setCrediti(crediti);

		System.out.println("Inserisci Data Esame: ");
		String data = input.nextLine();
		esame.setData(data);
		// voto
		
		System.out.println("Inserisci Voto Esame: ");
		int voto = input.nextInt();
		input.nextLine();
		esame.setVoto(voto);
		//lode
		
		System.out.println("Lode: 1) Si - 0) No");
		int lode = input.nextInt();
		input.nextLine();
		esame.setLode(lode);
		
		crud_services.jpaCreate(esame);
		studente.getListaEsami().add(esame);
		crud_services.jpaUpdate(studente);

		crud_services.closeLogicaJPA();
	}
	
	public static void readEsame(Scanner input){
		
		System.out.println("Cerca Esami Studente. ");
		System.out.println("Seleziona Matricola: ");
		int matricola = input.nextInt();
		input.nextLine();

		ServicesCrud crud_services = new ServicesCrud("jpa-example");

		Query jpaRead = crud_services.jpaRead("SELECT s FROM Studente s WHERE matricola = " + matricola);

		Studente studente = (Studente) jpaRead.getSingleResult();
		System.out.println(studente.getListaEsami());
		crud_services.closeLogicaJPA();
	}
	
	public static void updateEsame(Scanner input){
		
		System.out.println("Modifica Studente. ");
		System.out.println("Seleziona Matricola: ");
		int matricola = input.nextInt();
		input.nextLine();

		ServicesCrud crud_services = new ServicesCrud("jpa-example");
		Query jpaRead = crud_services.jpaRead("SELECT s FROM Studente s WHERE matricola = " + matricola);
		
		Studente studente = (Studente) jpaRead.getSingleResult();
		System.out.println(studente.getListaEsami());

		
		System.out.println("Quale esame vuoi modificare?");
		int nEsame = input.nextInt();
		input.nextLine();
		
		System.out.println("Quale campo vuoi modificare?");
		System.out.println("1) Nome Esame - 2) Crediti - 3) Data Esame - 4) Voto - 5) Lode - 6) Uscire");
		int key = input.nextInt();
		input.nextLine();
		
		int uscita = 0;
		
		while (uscita != 6) {
			switch (key) {
			case 1://nome
				System.out.println("Modifica Nome Esame:");
				String nome = input.nextLine();
				studente.getListaEsami().get(nEsame).setNome(nome);
				break;

			case 2://crediti
				System.out.println("Modifica Crediti Esame:");
				int crediti = input.nextInt();
				input.nextLine();
				studente.getListaEsami().get(nEsame).setCrediti(crediti);
				break;

			case 3://data
				System.out.println("Modifica Data Esame:");
				String data = input.nextLine();
				studente.getListaEsami().get(nEsame).setData(data);
				break;
				
			case 4://voto
				System.out.println("Modifica Voto Esame:");
				int voto = input.nextInt();
				input.nextLine();
				studente.getListaEsami().get(nEsame).setVoto(voto);

				break;
				
			case 5://lode
				System.out.println("Modifica Lode Esame:");
				int lode = input.nextInt();
				input.nextLine();
				studente.getListaEsami().get(nEsame).setVoto(lode);
				
				break;
				
			case 6:
				uscita = 6;
				break;
			}
		}
		crud_services.jpaUpdate(studente);
		System.out.println("Esami Studente Aggiornati.");
		crud_services.closeLogicaJPA();
	}
	
	public static void deleteEsame(Scanner input){
		
		System.out.println("Elimina Esame Studente. ");
		System.out.println("Seleziona Matricola: ");
		int matricola = input.nextInt();
		input.nextLine();

		ServicesCrud crud_services = new ServicesCrud("jpa-example");
		Query jpaRead = crud_services.jpaRead("SELECT s FROM Studente s WHERE matricola = " + matricola);
		
		Studente studente = (Studente) jpaRead.getSingleResult();
		System.out.println(studente.getListaEsami());

		//devo creare una select per l'ID di quell'esame
		System.out.println("Quale esame vuoi eliminare?");
		int nEsame = input.nextInt();
		input.nextLine();
		jpaRead = crud_services.jpaRead("SELECT e FROM Esame e WHERE id = " + nEsame);
		Esame esame = (Esame) jpaRead.getSingleResult();
		
		crud_services.jpaDelete(esame);
		System.out.println("Studente Eliminato.");
		crud_services.closeLogicaJPA();
	}
	
	
}
