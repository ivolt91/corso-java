package JBAGestioneEsami.JBAGestioneEsami;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "ESAME", schema = "CORSO")
public class Esame {
	@Id
	@GeneratedValue
	private int id;
	private String nome;
	private int crediti;
	private String data;
	private int voto;
	private int lode;
	
	
	
	
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public int getVoto() {
		return voto;
	}
	public void setVoto(int voto) {
		this.voto = voto;
	}
	public int getLode() {
		return lode;
	}
	public void setLode(int lode) {
		this.lode = lode;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getCrediti() {
		return crediti;
	}
	public void setCrediti(int crediti) {
		this.crediti = crediti;
	}
	@Override
	public String toString() {
		return "Esame [id=" + id + ", nome=" + nome + ", crediti=" + crediti + ", data=" + data + ", voto=" + voto
				+ ", lode=" + lode + "]";
	}

	
	
	
}
