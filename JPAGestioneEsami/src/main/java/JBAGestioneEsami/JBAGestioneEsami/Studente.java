package JBAGestioneEsami.JBAGestioneEsami;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

@Entity
@Table(name = "STUDENTE", schema = "CORSO")
public class Studente {
	@Id
	@GeneratedValue
	private int matricola;
	private String nome;
	private String cognome;
	private String dataDiNascita;
	
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
	  @JoinTable(name = "ESAMISVOLTI", schema = "CORSO", joinColumns = {
	      @JoinColumn(name = "STUDENTI_ID") }, inverseJoinColumns = { @JoinColumn(name = "ESAME_ID") })
	
	private List<Esame> listaEsami = new ArrayList<>();
	
	
	
	
	
	
	public List<Esame> getListaEsami() {
		return listaEsami;
	}
	public void setListaEsami(List<Esame> listaEsami) {
		this.listaEsami = listaEsami;
	}
	public int getMatricola() {
		return matricola;
	}
	public void setMatricola(int matricola) {
		this.matricola = matricola;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getDataDiNascita() {
		return dataDiNascita;
	}
	public void setDataDiNascita(String dataDiNascita) {
		this.dataDiNascita = dataDiNascita;
	}
	@Override
	public String toString() {
		return "Studente [matricola=" + matricola + ", nome=" + nome + ", cognome=" + cognome + ", dataDiNascita="
				+ dataDiNascita + ", esami=" + listaEsami + "]";
	}
	
	
	
	
	
	
	

}
